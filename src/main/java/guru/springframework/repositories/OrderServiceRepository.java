package guru.springframework.repositories;

import guru.springframework.domain.Order;
import org.springframework.data.repository.CrudRepository;

/**
 * Nemanja.Tomanovic created on 08-Apr-20
 **/
public interface OrderServiceRepository extends CrudRepository<Order, Integer> {
}
